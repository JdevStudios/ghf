FROM python:3.9 
# Or any preferred Python version.
ADD ./* .
RUN pip install chromedriver-autoinstaller selenium webdriver-manager fake_useragent 2captcha-python stem socks pyvirtualdisplay mailtm vosk ibm_cloud_sdk_core ibm_watson imap_tools mimesis TwoCaptcha undetected_chromedriver==3.1.5.post4 urlextract pydub SpeechRecognition
RUN apt update -y
RUN apt-get install -y ffmpeg xvfb 
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install
CMD ["python", "./sign_up.py"] 
